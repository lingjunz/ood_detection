from sklearn.metrics import roc_curve,auc,roc_auc_score,average_precision_score

import numpy as np

def get_metric_scores(y_true, y_score,tpr_level):
    
    fpr, tpr, thresholds = roc_curve(y_true, y_score)
    auroc = auc(fpr, tpr)
    tpr95_pos = np.abs(tpr - tpr_level).argmin()
    tnr_at_tpr95 = 1. - fpr[tpr95_pos] 
    aupr = average_precision_score(y_true, y_score)
    results = {"TNR":tnr_at_tpr95,'AUROC':auroc,'AUPR':aupr,"TNR_threshold":thresholds[tpr95_pos],
               'FPR':fpr,'TPR':tpr,"threshold":thresholds }
    
    return results
    
    

