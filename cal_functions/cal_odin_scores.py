import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd.gradcheck import zero_gradients

from torch.autograd import Variable

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

cifar10_mean = [0.4914,0.4822,0.4465]
cifar10_std = [0.2470,0.2435,0.2616]


def update_gradient(gradient,data_type):
    if data_type == "cifar10":
        gradient.index_copy_(1, torch.LongTensor([0]).cuda(), gradient.index_select(1, torch.LongTensor([0]).cuda()) / cifar10_std[0])
        gradient.index_copy_(1, torch.LongTensor([1]).cuda(), gradient.index_select(1, torch.LongTensor([1]).cuda()) / cifar10_std[1])
        gradient.index_copy_(1, torch.LongTensor([2]).cuda(), gradient.index_select(1, torch.LongTensor([2]).cuda()) / cifar10_std[2])
    else:
        pass
    # return gradient

def get_odin_scores(net, loader, data_type = "cifar10" ,magnitude = 0.0005, temper = 10, use_xent = False): 
    net.eval()
    _scores = []
    _preds = []
    _top2_diff = []

    for batch_idx,(img,target) in enumerate(loader):
        
        img = img.cuda()
        img_var = Variable(img,requires_grad=True)
        zero_gradients(img_var)
        output = net(img_var)/temper
        orig_preds = torch.argmax(output,dim=1)
        orig_xent =nn.CrossEntropyLoss()(output,orig_preds) #  F.cross_entropy
        orig_xent.backward()
        
        gradient =  torch.ge(img_var.grad.data, 0)
        gradient = (gradient.float() - 0.5) * 2

        if data_type == "cifar10":
            gradient.index_copy_(1, torch.LongTensor([0]).cuda(), gradient.index_select(1, torch.LongTensor([0]).cuda()) / cifar10_std[0])
            gradient.index_copy_(1, torch.LongTensor([1]).cuda(), gradient.index_select(1, torch.LongTensor([1]).cuda()) / cifar10_std[1])
            gradient.index_copy_(1, torch.LongTensor([2]).cuda(), gradient.index_select(1, torch.LongTensor([2]).cuda()) / cifar10_std[2])
        else:
            pass

        tempInputs = torch.add(img_var.data, gradient ,alpha = -magnitude)

        with torch.no_grad():
            output = net(Variable(tempInputs))/temper
            smax = to_np(F.softmax(output,dim=1))
            cur_preds = np.argmax(smax, axis=1)
            _preds.append(cur_preds)
            temp = np.sort(-1*smax)
            top2_diff = -1*(temp[:,0]- temp[:,1])
            _top2_diff.append(top2_diff)
            orig_scores = 1-np.max(smax,axis=1)
            _scores.append(orig_scores)
   
    return concat(_top2_diff),concat(_scores),concat(_preds)


