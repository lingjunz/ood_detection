import numpy as np
import torch
import torch.nn.functional as F

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def get_oe_scores(net, loader, use_xent = False): 
    net.eval()
    _scores = []
    _preds = []
    _top2_diff = []

    with torch.no_grad():
        for batch_idx,(data,target) in enumerate(loader):
            data = data.cuda()
            output = net(data)
            smax = to_np(F.softmax(output,dim=1))
            cur_preds = np.argmax(smax, axis=1)
            _preds.append(cur_preds)
            
            temp = np.sort(-1*smax)
            top2_diff = -1*(temp[:,0]- temp[:,1])
            _top2_diff.append(top2_diff)
            
            if use_xent:
                orig_scores = to_np((output.mean(1) - torch.logsumexp(output,dim=1)))
            else:
                orig_scores = 1-np.max(smax,axis=1)

            _scores.append(orig_scores)
            
    return concat(_top2_diff),concat(_scores),concat(_preds)


