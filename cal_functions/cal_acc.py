import numpy as np
import torch
import torch.nn.functional as F

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()


def cal_accuracy(net,test_loader,info=True):
    net.eval()
    _preds = []
    _pred_vectors = []
    correct = 0
    total_loss = 0.0
    right_conf = 0.0
    wrong_conf = 0.0
    total = len(test_loader.dataset)
    with torch.no_grad():
        for data,target in test_loader:
            target = target.long()
            data,target = data.cuda(),target.cuda()
            # forward
            output = net(data)
            # test loss
            loss = F.cross_entropy(output, target)
            total_loss += float(loss.data)
            # calculate 
            cur_pred_vectors = torch.softmax(output.data,1)
            msps, preds = torch.max(cur_pred_vectors, 1)
            correct += (preds == target).sum().item()
            right_conf += msps[preds == target].sum().item()
            wrong_conf += msps[preds != target].sum().item()
            # add results
            _preds.append(to_np(preds))
            _pred_vectors.append(to_np(cur_pred_vectors))
    wrong = total - correct
    correct_avg = np.round(100 * right_conf / correct , 2) if correct != 0 else -1
    wrong_avg =np.round(100 * wrong_conf / wrong , 2) if wrong != 0 else -1
    test_loss = total_loss / len(test_loader)
    test_acc = correct / len(test_loader.dataset)
    if info:
        print(">>>Accuracy:%.5f,Loss:%.4f,%d/%d"%(test_acc,test_loss,correct,total))
        print("correct avg confidence:{}\t wronf avg confidence:{}".format(correct_avg,wrong_avg))
    return test_acc, concat(_preds), concat(_pred_vectors)


#             _preds.append(smax) # return all predicted
#             temp = np.sort(-1*smax)
#             top1_conf = -1*(temp[:,0])#- temp[:,1])
#             _top1_conf.append(top1_conf)