import torch
import numpy as np
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from keras.datasets import cifar10,mnist
import os
import sklearn.covariance
import sys
sys.path.append("..")
# from utils.helper_function import *


concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()
import copy
def sample_estimator(model, model_name, data_type , num_classes, selected_idx, train_loader, save_path):
    """
    compute sample mean and precision (inverse of covariance)
    return: sample_class_mean: list of class mean
             precision: list of precisions
    """
    
    model.eval()
    group_lasso = sklearn.covariance.EmpiricalCovariance(assume_centered=False)
    correct, total = 0, 0
    num_output = len(selected_idx)
    num_sample_per_class = np.zeros(num_classes)
    list_features = []
    for i in range(num_output):
        temp_list = [0]*num_classes
        list_features.append(temp_list)
    with torch.no_grad():
        for data, target in train_loader:
            total += data.size(0)
            data = data.cuda()
            data = Variable(data)
            output, out_features, _ = model.feature_list(data) # out_features:5,2,64,32,32
            new_out_features = []
            for idx,cur in enumerate(out_features):
                if idx in selected_idx:
                    new_out_features.append(cur) 
            out_features = new_out_features

            # get hidden features
            for i in range(num_output):
                if len(out_features[i].shape) == 4:
                    out_features[i] = out_features[i].view(out_features[i].size(0), out_features[i].size(1), -1)
                    out_features[i] = torch.mean(out_features[i].data, 2) # 5,2,64
                 
            # compute the accuracy           
            cur_preds = np.argmax(to_np(output), axis=1)
            targets = target.numpy().squeeze()
            correct += np.sum(cur_preds==targets)
            # construct the sample matrix
            for i in range(data.size(0)):
                label = target[i]
                if num_sample_per_class[label] == 0:
                    out_count = 0
                    for out in out_features: #out_features: feature_num, batch_num, kernels,
                        list_features[out_count][label] = out[i].view(1, -1)
                        out_count += 1
                else:
                    out_count = 0
                    for out in out_features:
                        list_features[out_count][label] \
                        = torch.cat((list_features[out_count][label], out[i].view(1, -1)), 0)
                        out_count += 1                
                num_sample_per_class[label] += 1
        print('\n Training Accuracy:{:.2f}%({}/{})\n'.format(100. * correct / total,correct,total))

        sample_class_mean = []
        out_count = 0
        for i in range(num_output):
            num_feature = out_features[i].shape[1]
            temp_list = torch.Tensor(num_classes, int(num_feature)).cuda()
            for j in range(num_classes):
                temp_list[j] = torch.mean(list_features[out_count][j], 0)
            sample_class_mean.append(temp_list)
            out_count += 1

        #########mean of features computed successfully according to different labels########### 
        precision = []
        for k in range(num_output):
            X = 0
            for i in range(num_classes):
                if i == 0:
                    X = list_features[k][i] - sample_class_mean[k][i]
                else:
                    X = torch.cat((X, list_features[k][i] - sample_class_mean[k][i]), 0)    
            # find inverse            
            group_lasso.fit(X.cpu().numpy())
            temp_precision = group_lasso.precision_
            temp_precision = torch.from_numpy(temp_precision).float().cuda()
            precision.append(temp_precision)

    np.save(os.path.join(save_path,"{}_{}_mean.npy".format(data_type,model_name)),sample_class_mean)
    np.save(os.path.join(save_path,"{}_{}_precision.npy".format(data_type,model_name)),precision)
    print("[{},{}]save mean and precision statistics successfully!".format(data_type,model_name))
    return sample_class_mean, precision

# def get_raw_mahalanobis(model, selected_idx, test_loader, sample_mean, precision, magnitude=0.0001, data_type="mnist", num_classes = 10):
#     for i in range(num_output):
#         M_score = get_Mahalanobis_score(model, test_loader, num_classes, sample_mean, 
#                                                        precision, i, magnitude, data_type)
#         M_score = np.asarray(M_score, dtype=np.float32)
#         if i == 0:
#             Mahalanobis_score = M_score.reshape((M_score.shape[0], -1))
#         else:
#             Mahalanobis_score = np.concatenate((Mahalanobis_score, M_score.reshape((M_score.shape[0], -1))), axis=1)
#     return Mahalanobis_score
cifar10_mean = [0.4914,0.4822,0.4465]
cifar10_std = [0.2470,0.2435,0.2616]
def get_Mahalanobis_score(model, test_loader, num_classes, sample_mean, precision, selected_idx, magnitude, data_type = "cifar10"):
    '''
    Compute the proposed Mahalanobis confidence score on input dataset
    return: Mahalanobis score from layer_index
    '''
    model.eval()
    Mahalanobis = []
    
    for i in range(len(selected_idx)):
         Mahalanobis.append([])

    for data, _ in test_loader:
        
        data = data.cuda()
        data = Variable(data, requires_grad = True)
        _,out_features,_ = model.feature_list(data)

        new_out_features = []
        for i,cur in enumerate(out_features):
            if i in selected_idx:
                new_out_features.append(cur) 
        out_features = new_out_features

        for i,cur in enumerate(out_features):
            cur = cur.view(cur.size(0), cur.size(1), -1)
            cur = torch.mean(cur, 2)
            out_features[i] = cur
        out_features = np.array(out_features)
        # print("out features shape:{}".format(out_features.shape))

        # compute Mahalanobis score
        
        for idx in range(len(selected_idx)):
            gaussian_score = 0
            for i in range(num_classes):
                batch_sample_mean = sample_mean[idx][i]
                zero_f = out_features[idx].data - batch_sample_mean # batch_num * feature_num -> 256*64
                term_gau = -0.5*torch.mm(torch.mm(zero_f, precision[idx]), zero_f.t()).diag() # [batch_num]
                if i == 0:
    #                 print(term_gau.shape)
                    gaussian_score = term_gau.view(-1,1)
                else:
                    gaussian_score = torch.cat((gaussian_score, term_gau.view(-1,1)), 1) 
        
            # Input_processing
            sample_pred = gaussian_score.max(1)[1] # gaussian_score batch_num*class_num, 256*10
            batch_sample_mean = sample_mean[idx].index_select(0, sample_pred)
            zero_f = out_features[idx] - Variable(batch_sample_mean)
            pure_gau = -0.5*torch.mm(torch.mm(zero_f, Variable(precision[idx])), zero_f.t()).diag()
            loss = torch.mean(-pure_gau)
            loss.backward(retain_graph=True)

            gradient =  torch.ge(data.grad.data, 0)
            gradient = (gradient.float() - 0.5) * 2 # batch_num,channel,width,height
            if data_type == "cifar10":
                gradient.index_copy_(1, torch.LongTensor([0]).cuda(), gradient.index_select(1, torch.LongTensor([0]).cuda()) / cifar10_std[0])
                gradient.index_copy_(1, torch.LongTensor([1]).cuda(), gradient.index_select(1, torch.LongTensor([1]).cuda()) / cifar10_std[1])
                gradient.index_copy_(1, torch.LongTensor([2]).cuda(), gradient.index_select(1, torch.LongTensor([2]).cuda()) / cifar10_std[2])

            tempInputs = torch.add(data.data, gradient ,alpha = -magnitude)
            with torch.no_grad():
                
                _,noise_out_features,_ = model.feature_list(Variable(tempInputs))
                new_noise_out_features = []
                for i,cur in enumerate(noise_out_features):
                    if i in selected_idx:
                        new_noise_out_features.append(cur) 
                noise_out_features = new_noise_out_features
                for i,cur in enumerate(noise_out_features):
                    cur = cur.view(cur.size(0), cur.size(1), -1)
                    cur = torch.mean(cur, 2)
                    noise_out_features[i] = cur
                noise_out_features = np.array(noise_out_features)

                noise_gaussian_score = 0
                for i in range(num_classes):
                    batch_sample_mean = sample_mean[idx][i]
                    zero_f = noise_out_features[idx].data - batch_sample_mean
                    term_gau = -0.5*torch.mm(torch.mm(zero_f, precision[idx]), zero_f.t()).diag()
                    if i == 0:
                        noise_gaussian_score = term_gau.view(-1,1)
                    else:
                        noise_gaussian_score = torch.cat((noise_gaussian_score, term_gau.view(-1,1)), 1)      
                noise_gaussian_score, _ = torch.max(noise_gaussian_score, dim=1)
                Mahalanobis[idx].extend(noise_gaussian_score.cpu().numpy())
        
    Mahalanobis = np.array(Mahalanobis).transpose()
    return Mahalanobis

