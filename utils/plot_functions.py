import os
import torch
import numpy as np
import scipy.special as sc 
import seaborn as sns
sns.set()
import matplotlib 
# matplotlib.use('Agg')
from matplotlib import pyplot as plt
# %matplotlib inline

__all__ = ['show_hist','plot_12_samples','plot_2_distribution','plot_samples']

def show_hist(data, bin=40,range = (0,1), alpha=0.7, title='results'):
    print(title, 'Average:', np.average(data))
    plt.hist(data, bins=bin,range = range, facecolor="blue", edgecolor="black", alpha=alpha)
    plt.xlabel("interval")
    plt.ylabel("num")
    plt.title(title)
    plt.show()

def plot_12_samples(samples,cmap=None):
#     plt.figure(figsize=(6,8))
#     plt.subplots_adjust(left=0,bottom=0,top=1,right=1,wspace=0.1,hspace=0.1)
    for i in range(12):
        plt.subplot(3,4,i+1)
#         plt.imshow(samples[i],vmin=0, vmax=255,cmap="gray",aspect = "auto")
        if cmap is not None:
            plt.imshow(samples[i],vmin=0, vmax=255,cmap=cmap)
        else:
            plt.imshow(samples[i],vmin=0, vmax=255,cmap=cmap)
        plt.axis("off")
    plt.show()

def plot_2_distribution(id_dis,id_label,ood_dis,ood_label,title="results",range = (0,1),save_path = None):
    sns.set(color_codes=True)
    sns.distplot(id_dis,label = id_label,bins=40,kde=False,hist_kws={'range':range,'edgecolor':"black",'alpha':0.5})
    sns.distplot(ood_dis,label = ood_label,bins=40,kde=False,hist_kws={'range':range,'edgecolor':"black",'alpha':0.5})
    plt.legend()
    if save_path == "None":
        plt.title(title)
        plt.show() 
    else:
        # save_path = os.path.join(save_path,title)
        plt.savefig("{}.pdf".format(save_path),bbox_inches='tight',dpi=300)

def plot_samples(name,folder_path,datasets):
    for i,cur_dataset in enumerate(datasets):
        print("current_dataset:%s"%(cur_dataset.split("_")[1]))
        cur = np.load(os.path.join(folder_path,cur_dataset))
        print("shape:{}".format(cur.shape))
        indexes = np.random.choice(range(cur.shape[0]),10,replace=False)
        for j,index in enumerate(indexes):
            plt.subplot(len(datasets),10,i*10+j+1)
            if len(cur.shape) == 3:
                plt.imshow(cur[index].squeeze(),cmap="gray",vmin=0, vmax=255)
            else:
                plt.imshow(cur[index].squeeze(),cmap="gray",vmin=0, vmax=255)
            plt.axis("off")
    plt.suptitle("samples from {}-like dataset".format(name))
    plt.show()
    