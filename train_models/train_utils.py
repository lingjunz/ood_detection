import torch
import numpy as np
import torch.nn.functional as F
from progressbar import ProgressBar,Percentage,Bar,Timer,ETA,FileTransferSpeed

import torch.optim as optim

import sys
sys.path.append("../")

from models.densenet import densenet_cifar
from models.resnet import ResNet18
from models.wideresnet import WRN_40_4
from models.lenet import Lenet5,Lenet4,Lenet1
import os

def adjust_opt(optAlg, optimizer, epoch):
    if optAlg == 'sgd':
        if epoch == 60: lr = 1e-2
        elif epoch == 120: lr = 0.005
        elif epoch == 180: lr = 1e-3
        else: return

        for param_group in optimizer.param_groups:
            param_group['lr'] = lr

def select_model_optimizer(selection,opt,num_classes,dropRate=0.0):
    if selection == "resnet18":
        net = ResNet18(num_classes,dropRate)
    elif selection == "densenet":
        net = densenet_cifar(num_classes,dropRate)
    elif selection == "WRN_40_4": # 100 epoch
        net = WRN_40_4(num_classes,dropRate)
    elif selection == "lenet5":
        net = Lenet5(num_classes,dropRate)
    elif selection == "lenet4":
        net = Lenet4(num_classes,dropRate)
    elif selection == "lenet1":
        net = Lenet1(num_classes,dropRate)
    if torch.cuda.device_count()>1:
        net = torch.nn.DataParallel(net)
    net.cuda()
    if opt == 'sgd':
        optimizer = optim.SGD(net.parameters(), lr=1e-1,
                            momentum=0.9, weight_decay=1e-4)
    elif opt == 'adam':
        optimizer = optim.Adam(net.parameters(), weight_decay=1e-4)
    elif opt == 'rmsprop':
        optimizer = optim.RMSprop(net.parameters(), weight_decay=1e-4)
    return net,optimizer

def check_dir(save_path):
    if not os.path.exists(save_path):
        os.makedirs(save_path)
    if not os.path.isdir(save_path):
        raise Exception('%s is not a dir' % save_path)


def train_baseline(net,train_loader,optimizer,state):
    net.train()  # enter train mode
    loss_avg = 0.0
    correct  = 0
    total = len(train_loader)
    widgets = ['Training: ',Percentage(), ' ', Bar('#'),' ', Timer(),  
           ' ', ETA(), ' ', FileTransferSpeed()]  
    progress = ProgressBar(widgets=widgets, maxval=total)
    for data, target in progress(train_loader):
        data, target = data.cuda(), target.cuda()
        # forward
        output = net(data)
        # backward
#         scheduler.step()
        optimizer.zero_grad()
        loss = F.cross_entropy(output, target)
        loss.backward() 
        optimizer.step()
        # exponential moving average
        loss_avg = loss_avg * 0.8 + float(loss.data) * 0.2
        # accuracy
        pred = output.data.max(1)[1]
        correct += pred.eq(target.data).sum().item()

    progress.finish()
    state['train_loss'] = loss_avg
    state['train_accuracy'] = correct / len(train_loader.dataset)
    print("train_loss:{},train_accuracy;{}".format(state['train_loss'], state['train_accuracy']))

def train_oe(net,train_loader_in,train_loader_out,optimizer,state):
    net.train()  # enter train mode
    loss_avg = 0.0
    correct  = 0
    
    total = len(train_loader_in)
    widgets = ['Training: ',Percentage(), ' ', Bar('#'),' ', Timer(),  
           ' ', ETA(), ' ', FileTransferSpeed()]  

    progress = ProgressBar(widgets=widgets, maxval=total)
    # start at a random point of the outlier dataset; this induces more randomness without destroying locality
    train_loader_out.dataset.offset = np.random.randint(len(train_loader_out.dataset))
    train_loader = zip(train_loader_in, train_loader_out)
    
    for in_set, out_set in progress(train_loader):
        data = torch.cat((in_set[0], out_set[0]), 0)
        target = in_set[1]
        data, target = data.cuda(), target.cuda()
        # forward
        output = net(data)
        # backward
#         scheduler.step()
        optimizer.zero_grad()
        loss = F.cross_entropy(output[:len(in_set[0])], target)
        loss += 0.5 * -(output[len(in_set[0]):].mean(1) - torch.logsumexp(output[len(in_set[0]):], dim=1)).mean()
        loss.backward() 
        optimizer.step()
        # exponential moving average
        loss_avg = loss_avg * 0.8 + float(loss.data) * 0.2
        # accuracy
        pred = output[:len(in_set[0])].data.max(1)[1]
        correct += pred.eq(target.data).sum().item()
    progress.finish()
    state['train_loss'] = loss_avg
    state['train_accuracy'] = correct / len(train_loader_in.dataset)
    print("train_loss:{},train_accuracy;{}".format(state['train_loss'], state['train_accuracy']))

# test function
def test(net,test_loader,state):
    net.eval()
    loss_avg = 0.0
    correct = 0
    total = len(test_loader)
    widgets = ['Testing: ',Percentage(), ' ', Bar('#'),' ', Timer(),  
           ' ', ETA(), ' ', FileTransferSpeed()]  
    progress = ProgressBar(widgets=widgets, maxval=total)
    
    with torch.no_grad():
        for data, target in progress(test_loader):
            data, target = data.cuda(), target.cuda()
            # forward
            output = net(data)
          
            loss = F.cross_entropy(output, target) 
            # accuracy
            pred = output.data.max(1)[1]
            correct += pred.eq(target.data).sum().item()
            # test loss average
            loss_avg += float(loss.data)
    progress.finish()
    state['test_loss'] = loss_avg / len(test_loader)
#     state['test_loss'] = loss_avg
    state['test_accuracy'] = correct / len(test_loader.dataset)
    print("test_loss:{},test_accuracy;{}".format(state['test_loss'], state['test_accuracy']))

    

