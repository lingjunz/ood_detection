
from train_utils import train_baseline,train_oe,test,adjust_opt

import  torch
from  torch import nn
import torch.backends.cudnn as cudnn
# import torchvision.transforms as trn
import torchvision.datasets as dset
from torch.utils.data import DataLoader

from collections import defaultdict
import numpy as np
import os
import sys
sys.path.append("../")
from utils.ood_loader import OODImages
from utils.helper_function import make_dataloader

os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'#'0,1,2,3'
import time

from utils.configs import cifar_transforms_test,cifar_transforms_train,cifar100_transforms_train,cifar100_transforms_test
from train_utils import select_model_optimizer,check_dir


def train_model(selection = "resnet18",oe_tuned = False):

    # Configuration
    cudnn.benchmark = True  # fire on all cylinders
    num_classes = 100
    state = defaultdict()
    start_epoch = 0
    end_epoch = 200
    opt = "sgd"

    save_path = "CIFAR100_pytorch_{}_{}".format(selection,end_epoch)
    model_name = "{}_{}".format(selection, "oe" if oe_tuned else "baseline")
    
    train_transform = cifar100_transforms_train
    test_transform = cifar100_transforms_test
    
    train_data = dset.CIFAR100('/home/lingjun/ood_2020/data/pytorch_data/cifar100', train=True, transform=train_transform)
    test_data = dset.CIFAR100('/home/lingjun/ood_2020/data/pytorch_data/cifar100', train=False, transform=test_transform)
    train_loader = DataLoader(train_data,batch_size=250, shuffle=True, num_workers=4, pin_memory=True)
    test_loader = DataLoader(test_data,batch_size=256, shuffle=False, num_workers=4, pin_memory=True)
    
    if oe_tuned:
        ood_path ="/home/lingjun/ood_2020/data/training_ood_data/tinyimagenet_cifar_40.npy"
        ood_sampels = np.load(ood_path)
        print(ood_sampels.shape)
        train_loader_out = make_dataloader(ood_sampels,None,img_size=(32,32), batch_size=100, transform_test=train_transform)
        train_loader_in = train_loader
    
    # Load model structure
    net,optimizer = select_model_optimizer(selection,opt,num_classes)
    # Make save directory
    check_dir(save_path)
    with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'w') as f:
        f.write('epoch,time(s),train_loss,train_acc,test_loss,test_acc(%)\n')
    print('Beginning Training\n')
    # Main loop
    best_epoch = 0
    best_acc = 0.0
    for epoch in range(start_epoch, end_epoch):
        adjust_opt(opt, optimizer, epoch)
        if epoch % 50 == 0:
            print(">>>>LR:{}".format(optimizer.param_groups[0]['lr']))
        state['epoch'] = epoch
        begin_epoch = time.time()
        if oe_tuned:
            train_oe(net,train_loader_in,train_loader_out,optimizer,state)
        else:
            train_baseline(net,train_loader,optimizer,state)
        test(net,test_loader,state)
         # Save model
        if epoch==0:
            best_epoch = epoch
            best_acc = state['test_accuracy']
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            # network.module.state_dict()
            if torch.cuda.device_count()>1:
                print(">>> use {} gpus".format(torch.cuda.device_count()))
                torch.save(net.module.state_dict(),cur_save_path)
            else:
                torch.save(net.state_dict(),cur_save_path)
        cur_acc = state['test_accuracy']
        if cur_acc > best_acc:
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,epoch,cur_acc))
            # network.module.state_dict()
            if torch.cuda.device_count()>1:
                torch.save(net.module.state_dict(),cur_save_path)
            else:
                torch.save(net.state_dict(),cur_save_path)
            prev_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            if os.path.exists(prev_path): 
                os.remove(prev_path)
            best_epoch = epoch
            best_acc = cur_acc
        # Show results    
        with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'a') as f:
            f.write('%03d,%05d,%0.6f,%0.4f,%0.6f,%0.4f\n' % (
                (epoch + 1),
                time.time() - begin_epoch,
                state['train_loss'],
                state['train_accuracy'],
                state['test_loss'],
                state['test_accuracy'],
            ))
        print('|Epoch {0:3d} | Time {1:5d} | Train Loss {2:.4f}|  Train Acc {3:.4f} | Test Loss {4:.4f} | Test Acc {5:.4f}'.format(
        (epoch + 1),
        int(time.time() - begin_epoch),
        state['train_loss'],
        state['train_accuracy'],
        state['test_loss'],
        state['test_accuracy'])
        )

if __name__=="__main__":
#     train_model(selection = "VGG13",oe_tuned = True)
#     try:
    
#     train_model(selection = "densenet",oe_tuned = True)

    # try:
    #     train_model(selection = "resnet18",oe_tuned = False)
    # except:
    #     pass
    # try:
    #     train_model(selection = "densenet",oe_tuned = False)
    # except:
    #     pass
    try:
        train_model(selection = "resnet18",oe_tuned = True)
    except:
        pass
    try:
        train_model(selection = "densenet",oe_tuned = True)
    except:
        pass

    
    
    
    # train_model(selection = "WRN_40_4",oe_tuned = True)
    