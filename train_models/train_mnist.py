
from train_utils import train_baseline,train_oe,test,adjust_opt

import  torch
from  torch import nn
import torch.backends.cudnn as cudnn
import torchvision.transforms as trn
import torchvision.datasets as dset
from torch.utils.data import DataLoader

from collections import defaultdict
import numpy as np
import os


os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'#'0,1,2,3'
import sys
sys.path.append("../")
from utils.ood_loader import OODImages
from utils.helper_function import make_dataloader

import time
from train_utils import select_model_optimizer,check_dir


def train_model(selection = "lenet5",oe_tuned = False):
    # Configuration
    cudnn.benchmark = True  # fire on all cylinders
    num_classes = 10
    state = defaultdict()
    start_epoch = 0
    end_epoch = 200
    opt = "sgd"
    save_path = "FMNIST_pytorch_{}_{}".format(selection,end_epoch)
    model_name = "{}_{}".format(selection, "oe" if oe_tuned else "baseline")
    train_data = dset.FashionMNIST('/home/lingjun/ood_2020/data/pytorch_data/fashionMnist', train=True, transform=trn.ToTensor())
    test_data = dset.FashionMNIST('/home/lingjun/ood_2020/data/pytorch_data/fashionMnist', train=False, transform=trn.ToTensor())
    train_loader = torch.utils.data.DataLoader(train_data, batch_size=120, shuffle=True,num_workers=4, pin_memory=True)
    test_loader = torch.utils.data.DataLoader(test_data, batch_size=256, shuffle=False,num_workers=4, pin_memory=True)

#     x_train = np.load("./mnist_class_split/train_0_4_class_X.npy")
#     y_train = np.int64(np.load("./mnist_class_split/train_0_4_class_y.npy").squeeze())
#     train_loader = make_dataloader(x_train,y_train,img_size=(28,28),batch_size=256,transform_test=trn.ToTensor())
    
#     x_test = np.load("./mnist_class_split/test_0_4_class_X.npy")
#     y_test = np.int64(np.load("./mnist_class_split/test_0_4_class_y.npy").squeeze())
#     test_loader = make_dataloader(x_test,y_test,img_size=(28,28),batch_size=256,transform_test=trn.ToTensor())
    
    if oe_tuned:
        ood_transform = trn.Compose([ trn.RandomHorizontalFlip(),trn.ToTensor()])
        ood_path = "/home/lingjun/ood_2020/data/training_ood_data/kmnist_20000.npy"
        ood_samples = np.load(ood_path)
        train_loader_out = make_dataloader(ood_samples,None,img_size=(28,28),batch_size=40,transform_test=ood_transform)
        train_loader_in = train_loader

    # Load model structure
    net,optimizer = select_model_optimizer(selection,opt,num_classes)
    # Make save directory
    check_dir(save_path)

    with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'w') as f:
        f.write('epoch,time(s),train_loss,train_acc,test_loss,test_acc(%)\n')
    print('Beginning Training\n')
    # Main loop
    best_epoch = 0
    best_acc = 0.0
        
    for epoch in range(start_epoch, end_epoch):
        
        adjust_opt(opt, optimizer, epoch)
        if epoch % 50 == 0:
            print(">>>>LR:{}".format(optimizer.param_groups[0]['lr']))
        state['epoch'] = epoch
        begin_epoch = time.time()
        if oe_tuned:
            train_oe(net,train_loader_in,train_loader_out,optimizer,state)
        else:
            train_baseline(net,train_loader,optimizer,state)
        test(net,test_loader,state)
         # Save model
        if epoch==0:
            best_epoch = epoch
            best_acc = state['test_accuracy']
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            torch.save(net.state_dict(),cur_save_path)
        cur_acc = state['test_accuracy']
        if cur_acc > best_acc:
            cur_save_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,epoch,cur_acc))
            torch.save(net.state_dict(),cur_save_path)
            prev_path = os.path.join(save_path, '{}_epoch_{}_{}.pt'.format(model_name,best_epoch,best_acc))
            if os.path.exists(prev_path): 
                os.remove(prev_path)
            best_epoch = epoch
            best_acc = cur_acc
        # Show results    
        with open(os.path.join(save_path,  '{}_training_results.csv'.format(model_name)), 'a') as f:
            f.write('%03d,%05d,%0.6f,%0.4f,%0.6f,%0.4f\n' % (
                (epoch + 1),
                time.time() - begin_epoch,
                state['train_loss'],
                state['train_accuracy'],
                state['test_loss'],
                state['test_accuracy'],
            ))
        print('|Epoch {0:3d} | Time {1:5d} | Train Loss {2:.4f}|  Train Acc {3:.4f} | Test Loss {4:.4f} | Test Acc {5:.4f}'.format(
        (epoch + 1),
        int(time.time() - begin_epoch),
        state['train_loss'],
        state['train_accuracy'],
        state['test_loss'],
        state['test_accuracy'])
        )
    
if __name__=="__main__":

    train_model(selection = "lenet1",oe_tuned = False)
    
    train_model(selection = "lenet5",oe_tuned = False)

    train_model(selection = "lenet1",oe_tuned = True)
    
    train_model(selection = "lenet5",oe_tuned = True)
    
    
