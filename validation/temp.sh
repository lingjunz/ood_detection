python odin_lr.py -data_type mnist -model_name lenet1 -o ./valid_models

python odin_lr.py -data_type mnist -model_name lenet5 -o ./valid_models

python odin_lr.py -data_type cifar10 -model_name resnet18 -o ./valid_models

python odin_lr.py -data_type cifar10 -model_name densenet -o ./valid_models