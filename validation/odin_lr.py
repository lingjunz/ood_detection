
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'
import numpy as np
import argparse
import sys
sys.path.append("..")

from keras.datasets import cifar10,mnist
from cal_functions.cal_acc import cal_accuracy
from cal_functions.cal_odin_scores import get_odin_scores
from cal_functions.cal_metrics import get_metric_scores
from utils.helper_function import load_net
from utils.helper_function import make_dataloader
from utils.configs import transforms_dic,validation_ood_dic
from utils.plot_functions import plot_2_distribution
from utils.helper_function import merge_and_generate_labels,block_split
from sklearn.linear_model import LogisticRegressionCV
import joblib


magnitudes = [0.0,0.0001,0.0002,0.0004,0.0005,0.0006,0.0008,0.001,0.0012, 0.0014, 0.0016, 0.002,0.004,0.006,0.008,0.01,0.02,0.05]
tempers = [1,2,4,6,8,10,20,50]
# python odin_lr.py -data_type mnist -model_name lenet1 -o ./valid_models 
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a lr model for xent metric")
    parser.add_argument("-data_type",choices=['cifar10','mnist'],help="training dataset")
    parser.add_argument("-model_name",choices=['lenet1','lenet5','resnet18','densenet'],help="model structure")
    parser.add_argument("-oe",choices=[0,1],type=int,default=0,help="oe model or not")
    parser.add_argument("-o",help="output folder for lr models and figures")
    parser.add_argument("-metric",choices=["odin"],default="odin",help="train a lr model for the given metric")
    args = parser.parse_args()

    data_type = args.data_type
    model_name = args.model_name
    output = args.o
    oe = args.oe
    metric = args.metric

    if oe:
        model_name += "_oe"
    if not os.path.exists(output):
        os.makedirs(output)
    logfile = open(os.path.join(output,"log.txt"),"a+")
    odin_log_file = open("{}_{}_odin.log".format(data_type,model_name),"w+")
    net = load_net(data_type,model_name)
    if data_type == "mnist":
        test_transform = transforms_dic['mnist']
        (x_train,y_train),(x_test,y_test) = mnist.load_data()
        y_test = y_test.squeeze()
        img_size = (28,28)
        ood_samples = np.load(validation_ood_dic['mnist_like'])
        
    elif data_type == "cifar10":
        test_transform = transforms_dic['cifar10_test']
        (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        y_test = y_test.squeeze()
        img_size=(32,32)
        ood_samples = np.load(validation_ood_dic['cifar_like'])
    id_label = "id_train"
    ood_label = "ood_valid"
    idx = np.random.choice(len(x_train),len(ood_samples),replace=False)
    id_samples = x_train[idx]

    id_loader = make_dataloader(id_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)
    ood_loader = make_dataloader(ood_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)

    best_m = 0.0
    best_t = 1
    best_result = {"AUROC":0.0,"AUPR":0.0,"TNR":0.0}
    for temper in tempers:
        for magnitude in magnitudes:
            _,in_odin_scores,_ = get_odin_scores(net, id_loader, magnitude = magnitude, temper = temper ,data_type = data_type)
            _,ood_odin_scores,_ = get_odin_scores(net, ood_loader, magnitude = magnitude, temper = temper ,data_type = data_type)
            scores, labels = merge_and_generate_labels(ood_odin_scores, in_odin_scores)
            
            results = get_metric_scores(labels, scores.reshape(scores.shape[0],1),tpr_level=.95)

            auroc = np.round(results['AUROC']*100.,4)
            aupr = np.round(results['AUPR']*100.,4)
            tnr = np.round(results['TNR']*100.,4)
            tnr_threshold =  np.round(results['TNR_threshold']*100.,4)
            odin_log_file.write("magnitude:{m:6f}\ttemperature:{t:d}\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\tthreshold:{threshold:6.2f}\n".format(
                    m = magnitude,
                    t = temper,
                    auroc = auroc,
                    aupr = aupr,
                    tnr = tnr,
                    threshold = tnr_threshold,
                ))
            if results['TNR'] > best_result['TNR']:
                    best_result = results
                    best_m = magnitude
                    best_t = temper
    print(">>>> best param for {}:{},{}".format(model_name,best_m,best_t))
    odin_log_file.write(">>>> best param for {}:{},{}\n".format(model_name,best_m,best_t))
    odin_log_file.flush()
    odin_log_file.close()

    _,id_scores,_ = get_odin_scores(net, id_loader, magnitude = best_m, temper = best_t ,data_type = data_type)
    _,ood_scores,_ = get_odin_scores(net, ood_loader, magnitude = best_m, temper = best_t ,data_type = data_type)
    save_path = os.path.join(output,"orig_{}_{}_{}".format(data_type,model_name,metric))
    plot_2_distribution(
        id_scores,id_label,
        ood_scores,ood_label,
        None,None,save_path
    )
    scores, labels = merge_and_generate_labels(ood_scores, id_scores)
    results = get_metric_scores(labels, scores, tpr_level=0.95)
    print("[{}]orig validation for {} with {}".format(data_type,model_name,metric))
    print("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.write("[{}]orig validation for {} with {}\n".format(data_type,model_name,metric))
    logfile.write("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\n".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))

    X_train, Y_train, X_test, Y_test = block_split(scores, labels, train_num = 17500, partition = 20000)
    lr = LogisticRegressionCV(n_jobs=-1, cv=3, max_iter=1000).fit(X_train, Y_train)
    y_pred = lr.predict_proba(scores)[:, 1]
    save_path = os.path.join(output,"trans_{}_{}_{}".format(data_type,model_name,metric))
    plot_2_distribution(
        lr.predict_proba(id_scores.reshape(-1,1))[:, 1],
        id_label,
        lr.predict_proba(ood_scores.reshape(-1,1))[:, 1],
        ood_label,
        None,(0,1),save_path
    )
    results = get_metric_scores(labels,y_pred,tpr_level=0.95)
    print("[{}]trans validation for {} with {}".format(data_type,model_name,metric))
    print("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.write("[{}]trans validation for {} with {}\n".format(data_type,model_name,metric))
    logfile.write("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\n".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.flush()
    logfile.close()
    save_path = os.path.join(output,"{}_{}_{}".format(data_type,model_name,metric))
    joblib.dump(lr,"{}_lr.model".format(save_path))



