
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'
import numpy as np
import argparse
import sys
sys.path.append("..")

from keras.datasets import cifar10,mnist
from cal_functions.cal_acc import cal_accuracy
from cal_functions.cal_oe_scores import get_oe_scores
from cal_functions.cal_metrics import get_metric_scores
from utils.helper_function import load_net
from utils.helper_function import make_dataloader
from utils.configs import transforms_dic,validation_ood_dic
from utils.plot_functions import plot_2_distribution
from utils.helper_function import merge_and_generate_labels,block_split
from sklearn.linear_model import LogisticRegressionCV
import joblib

# python xent_lr.py -data_type mnist -model_name lenet5 -oe 0 -o ./valid_models -metric msp
# python xent_lr.py -data_type cifar10 -model_name resnet18 -oe 0 -o ./valid_models -metric msp
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a lr model for xent metric")
    parser.add_argument("-data_type",choices=['cifar10','mnist'],help="training dataset")
    parser.add_argument("-model_name",choices=['lenet1','lenet5','resnet18','densenet'],help="model structure")
    parser.add_argument("-oe",choices=[0,1],type=int,default=0,help="oe model or not")
    parser.add_argument("-o",help="output folder for lr models and figures")
    parser.add_argument("-metric",choices=["msp","xent"],help="train a lr model for the given metric")
    args = parser.parse_args()

    data_type = args.data_type
    model_name = args.model_name
    output = args.o
    oe = args.oe
    metric = args.metric
    
    if oe:
        model_name += "_oe"
    if not os.path.exists(output):
        os.makedirs(output)
    logfile = open(os.path.join(output,"log.txt"),"a+")
    net = load_net(data_type,model_name)
    if data_type == "mnist":
        test_transform = transforms_dic['mnist']
        (x_train,y_train),(x_test,y_test) = mnist.load_data()
        y_test = y_test.squeeze()
        img_size = (28,28)
        ood_samples = np.load(validation_ood_dic['mnist_like'])
        
    elif data_type == "cifar10":
        test_transform = transforms_dic['cifar10_test']
        (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        y_test = y_test.squeeze()
        img_size=(32,32)
        ood_samples = np.load(validation_ood_dic['cifar_like'])
    id_label = "id_train"
    ood_label = "ood_valid"
    idx = np.random.choice(len(x_train),len(ood_samples),replace=False)
    id_samples = x_train[idx]
    # check acc on test data
    id_dataloader = make_dataloader(x_test,y_test,img_size=img_size, batch_size=256, transform_test=test_transform)
    acc,_,_ = cal_accuracy(net,id_dataloader)

    id_dataloader = make_dataloader(id_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)
    ood_dataloader = make_dataloader(ood_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)
    if metric=="msp":
        _,id_scores,_ = get_oe_scores(net, id_dataloader)#, use_xent = True)
        _,ood_scores,_ = get_oe_scores(net, ood_dataloader)#, use_xent = True)
    else:
        _,id_scores,_ = get_oe_scores(net, id_dataloader, use_xent = True)
        _,ood_scores,_ = get_oe_scores(net, ood_dataloader, use_xent = True)

    save_path = os.path.join(output,"orig_{}_{}_{}".format(data_type,model_name,metric))
    plot_2_distribution(
        id_scores,id_label,
        ood_scores,ood_label,
        None,None,save_path
    )
    scores, labels = merge_and_generate_labels(ood_scores, id_scores)
    results = get_metric_scores(labels, scores, tpr_level=0.95)

    print("[{}]orig validation for {} with {}".format(data_type,model_name,metric))
    print("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.write("[{}]orig validation for {} with {}\n".format(data_type,model_name,metric))
    logfile.write("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\n".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))

    X_train, Y_train, X_test, Y_test = block_split(scores, labels, train_num = 17500, partition = 20000)
    lr = LogisticRegressionCV(n_jobs=-1, cv=3, max_iter=1000).fit(X_train, Y_train)
    y_pred = lr.predict_proba(scores)[:, 1]

    save_path = os.path.join(output,"trans_{}_{}_{}".format(data_type,model_name,metric))
    plot_2_distribution(
        lr.predict_proba(id_scores.reshape(-1,1))[:, 1],
        id_label,
        lr.predict_proba(ood_scores.reshape(-1,1))[:, 1],
        ood_label,
        None,(0,1),save_path
    )
    
    results = get_metric_scores(labels,y_pred,tpr_level=0.95)
    print("[{}]trans validation for {} with {}".format(data_type,model_name,metric))
    print("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.write("[{}]trans validation for {} with {}\n".format(data_type,model_name,metric))
    logfile.write("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\n".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.flush()
    logfile.close()
    save_path = os.path.join(output,"{}_{}_{}".format(data_type,model_name,metric))
    joblib.dump(lr,"{}_lr.model".format(save_path))
    