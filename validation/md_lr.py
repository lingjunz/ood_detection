
import os
os.environ['CUDA_VISIBLE_DEVICES'] = '1'
import numpy as np
import argparse
import sys
sys.path.append("..")
from torch.autograd import Variable
import torch
from keras.datasets import cifar10,mnist
from cal_functions.cal_acc import cal_accuracy
from cal_functions.cal_metrics import get_metric_scores
from utils.helper_function import load_net
from utils.helper_function import make_dataloader
from utils.configs import transforms_dic,validation_ood_dic
from utils.plot_functions import plot_2_distribution
from utils.helper_function import merge_and_generate_labels,block_split
from utils.helper_function import load_torch_data
from utils.md_utils import sample_estimator,get_Mahalanobis_score
from utils.configs import selected_features

from sklearn.linear_model import LogisticRegressionCV
import joblib
import copy

# python md_lr.py -data_type mnist -model_name lenet1 -o ./valid_models 
# python md_lr.py -data_type mnist -model_name lenet5 -o ./valid_models 
# python md_lr.py -data_type cifar10 -model_name resnet18 -o ./valid_models 
# python md_lr.py -data_type cifar10 -model_name densenet -o ./valid_models 

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Train a lr model for xent metric")
    parser.add_argument("-data_type",choices=['cifar10','mnist'],help="training dataset")
    parser.add_argument("-model_name",choices=['lenet1','lenet5','resnet18','densenet'],help="model structure")
    parser.add_argument("-oe",choices=[0,1],type=int,default=0,help="oe model or not")
    parser.add_argument("-o",help="output folder for lr models and figures")
    parser.add_argument("-metric",choices=["md"],default="md",help="train a lr model for the given metric")
    args = parser.parse_args()
    num_classes = 10
    data_type = args.data_type
    model_name = args.model_name
    output = args.o
    oe = args.oe
    metric = args.metric
    device = torch.device('cuda:0')
    if oe:
        model_name += "_oe"
    if not os.path.exists(output):
        os.makedirs(output)
    logfile = open(os.path.join(output,"log.txt"),"a+")
    md_log_file = open("{}_{}_md.log".format(data_type,model_name),"w+")
    net = load_net(data_type,model_name)
    if data_type == "mnist":
        m_list = [i*0.002 for i in range(50)]
        test_transform = transforms_dic['mnist']
        img_size=(28,28)
        rand_img_size = (2,1,28,28)
        ood_samples = np.load(validation_ood_dic['mnist_like'])
    elif data_type == "cifar10":
        m_list = [0, 0.0001] + [0.0002*i for i in range(1,6)] + \
           [0.0012, 0.0014, 0.0016, 0.002, 0.003, 0.004, 0.006, 0.008,0.01] + \
           [ 0.012,0.014,0.016,0.018,0.02]
        test_transform = transforms_dic['cifar10_test']
        # (x_train,y_train),(x_test,y_test) = cifar10.load_data()
        # y_test = y_test.squeeze()
        img_size=(32,32)
        rand_img_size = (2,3,32,32)
        ood_samples = np.load(validation_ood_dic['cifar_like'])

    chosen_features = selected_features[model_name]
    x_train,y_train = load_torch_data(data_type,net,test_transform,from_train=True,sample_num=0)
    train_loader = make_dataloader(x_train,y_train,img_size=img_size,batch_size=256,transform_test=test_transform)
    mean_precision_path = "./validation/md_mean_precision"
    if not os.path.exists(mean_precision_path):
        os.makedirs(mean_precision_path)
    cur_mean_path = os.path.join(mean_precision_path,"{}_{}_mean.npy".format(data_type,model_name))
    cur_precision_path = os.path.join(mean_precision_path,"{}_{}_precision.npy".format(data_type,model_name))

    temp_x = torch.rand(rand_img_size).to(device)
    temp_x = Variable(temp_x)
    _,temp_list, name_list = net.feature_list(temp_x)
    num_output = len(chosen_features)
    selected_idx =[]
    count = 0
    for idx,out in enumerate(temp_list):
        if name_list[idx] in chosen_features:
            print(out.size())
            selected_idx.append(idx)
            count += 1
    selected_idx = np.array(selected_idx)

    if not os.path.exists(cur_mean_path):
        print('get sample mean and covariance',selected_idx)
        sample_mean, precision = sample_estimator(net, model_name, data_type, num_classes, selected_idx, train_loader, mean_precision_path)
        sample_mean = list(sample_mean)
        precision = list(precision)
    else:
        sample_mean = np.load(cur_mean_path, allow_pickle=True).tolist()
        precision = np.load(cur_precision_path, allow_pickle=True).tolist()
        print("Load sampel mean and precision successfully!")
    
    id_label = "id_train"
    ood_label = "ood_valid"
    idx = np.random.choice(len(x_train),len(ood_samples),replace=False)
    id_samples = x_train[idx]
    id_loader = make_dataloader(id_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)
    ood_loader = make_dataloader(ood_samples,None,img_size=img_size, batch_size=256, transform_test=test_transform)
    best_m = -1
    best_lr = None
    best_result = {"AUROC":0.0,"AUPR":0.0,"TNR":0.0}
    for i,magnitude in enumerate(m_list): 
        in_md_scores = get_Mahalanobis_score(net, id_loader,num_classes, sample_mean, precision, selected_idx, magnitude, data_type)
        ood_md_scores = get_Mahalanobis_score(net, ood_loader,num_classes, sample_mean, precision, selected_idx, magnitude, data_type)
        
        if i==0:
            print("Mahalanobis features shape:{}".format(in_md_scores.shape))

        scores, labels = merge_and_generate_labels(ood_md_scores, in_md_scores)
        X_train, Y_train, X_test, Y_test = block_split(scores, labels, train_num = 17500, partition = 20000)
        lr = LogisticRegressionCV(n_jobs=-1,cv=3,max_iter=1000).fit(X_train, Y_train)

        y_pred = lr.predict_proba(X_test)[:, 1]
        results = get_metric_scores(Y_test,y_pred,0.95)

        auroc = np.round(results['AUROC']*100.,4)
        aupr = np.round(results['AUPR']*100.,4)
        tnr = np.round(results['TNR']*100.,4)
        tnr_threshold =  np.round(results['TNR_threshold']*100.,4)
        md_log_file.write("magnitude:{m:6f}\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\tthreshold:{threshold:6.2f}\n".format(
                m = magnitude,
                auroc = auroc,
                aupr = aupr,
                tnr = tnr,
                threshold = tnr_threshold,
            ))
        if results['TNR'] > best_result['TNR']:
                best_result = results
                best_m = magnitude
                best_lr = copy.deepcopy(lr)
        md_log_file.flush()
    print(">>>> best param for {}:{}".format(model_name,best_m))
    md_log_file.write(">>>> best param for {}:{}\n".format(model_name,best_m))
    md_log_file.flush()
    md_log_file.close()

    id_scores = get_Mahalanobis_score(net, id_loader, num_classes, sample_mean, precision, selected_idx, best_m, data_type)
    ood_scores = get_Mahalanobis_score(net, ood_loader, num_classes, sample_mean, precision, selected_idx, best_m, data_type)
    scores, labels = merge_and_generate_labels(ood_scores, id_scores)
    
    y_pred = best_lr.predict_proba(scores)[:, 1]
    save_path = os.path.join(output,"trans_{}_{}_{}".format(data_type,model_name,metric))
    plot_2_distribution(
        best_lr.predict_proba(id_scores)[:, 1],
        id_label,
        best_lr.predict_proba(ood_scores)[:, 1],
        ood_label,
        None,(0,1),save_path
    )
    results = get_metric_scores(labels,y_pred,tpr_level=0.95)
    print("[{}]trans validation for {} with {}".format(data_type,model_name,metric))
    print("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.write("[{}]trans validation for {} with {}\n".format(data_type,model_name,metric))
    logfile.write("\tAUROC:{auroc:6.2f}\tAUPR:{aupr:6.2f}\tTNR:{tnr:6.2f}\n".format(
        auroc = results['AUROC']*100.,
        aupr = results['AUPR']*100.,
        tnr = results['TNR']*100.,
    ))
    logfile.flush()
    logfile.close()
    save_path = os.path.join(output,"{}_{}_{}".format(data_type,model_name,metric))
    joblib.dump(best_lr,"{}_lr.model".format(save_path))