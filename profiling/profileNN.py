import torch
import pickle
import numpy as np
import collections
from keras.datasets import cifar10,mnist,fashion_mnist,cifar100
import os 
os.environ['CUDA_DEVICE_ORDER'] = 'PCI_BUS_ID'
os.environ['CUDA_VISIBLE_DEVICES'] = '1'


import sys
sys.path.append("../")
from utils.helper_function import load_net, make_dataloader
from utils.configs import transforms_dic

concat = lambda x: np.concatenate(x,axis=0)
to_np = lambda x: x.data.cpu().numpy()

def profileDNN(net,dataloader):
    net.eval()
    cov_dict = collections.OrderedDict()
    profile_num = len(train_loader.dataset)
    for i,(data,_) in enumerate(dataloader):
        data = data.to(device)
        _, out_list, name_list = net.feature_list(data)
        cur_batch = len(data)
        finished_num = i * dataloader.batch_size 
        
        if i == 0:
            neurons = [item.size(1) for item in out_list]
            print("layer_name",name_list)
            print("neuron_num",neurons)
            print("total:",np.sum(neurons))
            for temp in range(len(out_list)):
                print(out_list[temp].shape)
        if i % 100 == 0:
            print("* finish {}/{} samples".format(finished_num,profile_num))
            
        for layer_id in range(len(out_list)):
            cur_layer = name_list[layer_id]
            cur_neurons = out_list[layer_id].size(1)
            if len(out_list[layer_id].shape)==4:
                neurons_mean = to_np(out_list[layer_id]).mean(axis=(0,2,3)) # batch_num,cur_neurons
                neurons_max = to_np(out_list[layer_id]).mean(axis=(2,3)).max(axis=0)
                neurons_min = to_np(out_list[layer_id]).mean(axis=(2,3)).min(axis=0)
                neurons_square = np.sum((to_np(out_list[layer_id]).mean(axis=(2,3))**2),axis=0)
            else:
                neurons_mean = to_np(out_list[layer_id]).mean(axis=0) # batch_num,cur_neurons
                neurons_max = to_np(out_list[layer_id]).max(axis=0)
                neurons_min = to_np(out_list[layer_id]).min(axis=0)
                neurons_square = np.sum((to_np(out_list[layer_id])**2),axis=0)
                
                
            for neuron_id in range(cur_neurons):
                
                if (cur_layer,neuron_id) not in cov_dict:
                    # [mean_value_new, squared_mean_value, standard_deviation, lower_bound, upper_bound]
                    cov_dict[(cur_layer, neuron_id)] = [0.0, 0.0, 0.0, None, None]
                
                profile_data_list = cov_dict[(cur_layer, neuron_id)]
                mean_value = profile_data_list[0]
                squared_mean_value = profile_data_list[1]
                lower_bound = profile_data_list[3]
                upper_bound = profile_data_list[4]
                
                total_mean_value = mean_value * finished_num
                total_squared_mean_value = squared_mean_value * finished_num
                mean_value_new = (neurons_mean[neuron_id]*cur_batch + total_mean_value) / (finished_num + cur_batch)
                squared_mean_value =  (neurons_square[neuron_id] + total_squared_mean_value) / (finished_num + cur_batch)
                
                standard_deviation = np.math.sqrt(abs(squared_mean_value - mean_value_new * mean_value_new))
                if (lower_bound is None) and (upper_bound is None):
                        lower_bound = neurons_min[neuron_id]
                        upper_bound = neurons_max[neuron_id]
                else:
                    if neurons_min[neuron_id] < lower_bound:
                        lower_bound = neurons_min[neuron_id]

                    if neurons_max[neuron_id] > upper_bound:
                        upper_bound = neurons_max[neuron_id]
            
            
                profile_data_list[0] = mean_value_new
                profile_data_list[1] = squared_mean_value
                profile_data_list[2] = standard_deviation
                profile_data_list[3] = lower_bound
                profile_data_list[4] = upper_bound
                cov_dict[(cur_layer, neuron_id)] = profile_data_list    
                
    print(profile_num,finished_num+cur_batch)
    assert(profile_num==finished_num+cur_batch)
        
    return cov_dict

def dump(cov_dict, output_file):
    print("* profiling neuron size:", len(cov_dict.items()))
#     for item in cov_dict.items():
#         print(item)
    pickle_out = open(output_file, "wb")
    pickle.dump(cov_dict, pickle_out)
    pickle_out.close()

    print("write out profiling coverage results to ", output_file)
    print("done.")

def load_pickle(filepath):
    profile_dict = pickle.load(open(filepath, 'rb'))
    print("Load {} successfully!".format(filepath))
    print("* profiling neuron size:", len(profile_dict.items()))
    return profile_dict


import argparse
import os

# python profileNN.py -net densenet -oe 1 -o ./net_profiling -data_type cifar10
if __name__=="__main__":
    
    parser = argparse.ArgumentParser(description="Profile Neural Network with Pytorch")
    parser.add_argument('-net',choices=['densenet','resnet18','lenet5','lenet1'],help="target model to be profiled")
    parser.add_argument('-oe',choices=[0,1],type=int,help="choose to profile oe model or not")
    parser.add_argument("-o",default="./profiling",help="folder path to store profiling file")
    parser.add_argument("-data_type",choices=['cifar10','mnist','fmnist','cifar100'],help="choose correct data type")
    args = parser.parse_args()
    
    model_name = args.net
    oe_model = args.oe
    output_path = args.o
    data_type = args.data_type
    output_path = os.path.join(output_path,data_type)
    if not os.path.exists(output_path):
        os.makedirs(output_path)
        
    if oe_model:
        model_name += "_oe"
        
    device = torch.device('cuda:0')
    net = load_net(data_type,model_name,device=device)
    if data_type == "mnist":
        test_transform = transforms_dic['mnist']
        (x_train,y_train),(_,_) = mnist.load_data()
        batch_num = 256
        img_size = (28,28)
        total_size = 60000
    elif data_type == "fmnist":
        test_transform = transforms_dic['mnist']
        (x_train,y_train),(_,_) = fashion_mnist.load_data()
        batch_num = 256
        img_size = (28,28)
        total_size = 60000
    
    elif data_type == "cifar10":
        test_transform = transforms_dic['cifar10_test']
        (x_train,y_train),(_,_) = cifar10.load_data()
        batch_num = 50
        img_size = (32,32)
        total_size = 50000
    elif data_type == "cifar100":
        test_transform = transforms_dic['cifar100_test']
        (x_train,y_train),(_,_) = cifar100.load_data()
        batch_num = 50
        img_size = (32,32)
        total_size = 50000
    
    train_loader = make_dataloader(x_train,y_train.squeeze(),img_size=img_size, 
                               batch_size=batch_num, transform_test=test_transform)
    
    cov_dict = profileDNN(net,train_loader)
    
    save_path = os.path.join(output_path,"{}_0_{}".format(model_name,total_size))
    
    dump(cov_dict, save_path+".pickle")

    
    log = open(save_path+".log","w+")
    for key in cov_dict.keys():
        log.write("{}:{}\n".format(key,cov_dict[key]))
    log.flush()
    log.close()