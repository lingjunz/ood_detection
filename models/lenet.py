import  torch
from   torch import nn
import torch.nn.functional as F



class Lenet5(nn.Module):

    def __init__(self, num_classes=10, dropRate=0.0):
        super(Lenet5, self).__init__()
        self.conv_1 = nn.Sequential(nn.Conv2d(1, 6, kernel_size=5, stride=1, padding=0))
 
        self.conv_2 =nn.Sequential( nn.Conv2d(6, 16, kernel_size=5, stride=1, padding=0))
        self.avgpool = nn.AvgPool2d(kernel_size=2, stride=2, padding=0)
          
        
        self.fc_1 = nn.Linear(4*4*16, 120)
        self.fc_2 = nn.Linear(120, 84)
        self.relu = nn.ReLU()
        self.fc_3 = nn.Linear(84, num_classes)
        self.dropout = dropRate

     
    def forward(self, x):
        batchsz = x.size(0)

        x = self.avgpool(self.relu(self.conv_1(x)))

        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)

        x = self.avgpool(self.relu(self.conv_2(x)))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)

        x = x.view(batchsz, 4*4*16)

        x = self.fc_1(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)

        x = self.fc_2(self.relu(x))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
            
        logits = self.fc_3(self.relu(x))
        return logits

    # function to extract the multiple features(before activations)
    def feature_list(self, x):
        batchsz = x.size(0)
        out_list = []
        name_list = ["conv1","conv1_act","conv2","conv2_act","fc1","fc1_act","fc2","fc2_act","logits"]
        
        x = self.conv_1(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
  
        x = self.conv_2(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
       
        x = x.view(batchsz, 4*4*16)
        x = self.fc_1(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)

        x = self.fc_2(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)

        logits = self.fc_3(x)
        out_list.append(logits)

        return logits, out_list, name_list
    
    
    
class Lenet4(nn.Module):
    """
    for mnist dataset.
    """
    def __init__(self, num_classes=10, dropRate=0.0):
        super(Lenet4, self).__init__()
        self.conv_1 = nn.Conv2d(1, 6, kernel_size=5, stride=1, padding=0)
        self.conv_2 = nn.Conv2d(6, 16, kernel_size=5, stride=1, padding=0)
        self.avgpool = nn.AvgPool2d(kernel_size=2, stride=2, padding=0)
 
        self.fc_1 = nn.Linear(4*4*16, 84)
        self.relu = nn.ReLU()
        self.fc_2 = nn.Linear(84, num_classes)
        self.dropout = dropRate
           
 
    def forward(self, x):
        batchsz = x.size(0)
        x = self.avgpool(self.relu(self.conv_1(x)))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        x = self.avgpool(self.relu(self.conv_2(x)))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        x = x.view(batchsz, 4*4*16)
        x = self.fc_1(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        logits = self.fc_2(self.relu(x))
        return logits
    
    def feature_list(self, x):
        batchsz = x.size(0)
        out_list = []
        name_list = ["conv1","conv1_act","conv2","conv2_act","fc1","fc1_act","logits"]
        x = self.conv_1(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
  
        x = self.conv_2(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        out_list.append(x)
       
        x = x.view(batchsz, 4*4*16)
        x = self.fc_1(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        logits = self.fc_2(x)
        out_list.append(logits)

        return logits, out_list, name_list
    

class Lenet1(nn.Module):
    
    def __init__(self,num_classes=10, dropRate=0.0):
        super(Lenet1, self).__init__()
        
        self.conv_1 = nn.Sequential(nn.Conv2d(1, 4, kernel_size=5, stride=1, padding=0))
        self.conv_2 = nn.Sequential(nn.Conv2d(4, 12, kernel_size=5, stride=1, padding=0))
        self.avgpool = nn.AvgPool2d(kernel_size=2, stride=2, padding=0)
        self.relu = nn.ReLU()
        self.fc_1 = nn.Linear(4*4*12, num_classes)
        self.dropout = dropRate
    
    def forward(self, x):
        batchsz = x.size(0)
        x = self.avgpool(self.relu(self.conv_1(x)))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        x = self.avgpool(self.relu(self.conv_2(x)))
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        x = x.view(batchsz, 4*4*12)
        logits = self.fc_1(x)
        return logits

    # function to extract the multiple features(before activations)
    def feature_list(self, x):
        batchsz = x.size(0)
        out_list = []
        name_list = ["conv1","conv1_act","conv2","conv2_act","logits"]
        
        x = self.conv_1(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)

        x = self.conv_2(x)
        out_list.append(x)
        x = self.relu(x)
        out_list.append(x)
        x = self.avgpool(x)
        if self.dropout > 0:
            x = F.dropout(x,p = self.dropout)
        
        x = x.view(batchsz, 4*4*12)
        logits = self.fc_1(x)
        out_list.append(logits)
        
        return logits, out_list, name_list 
    
    
    

    
    